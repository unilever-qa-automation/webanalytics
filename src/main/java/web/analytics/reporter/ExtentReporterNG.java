package web.analytics.reporter;

import java.util.List;

import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.xml.XmlSuite;

import web.analytics.common.Constants;

public class ExtentReporterNG implements IReporter {

	@Override
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
		ComplexReportFactory.closeReport();
		System.out.println("Report Generated: " + Constants.REPORT_PATH);
	}

}
